<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\DocumentOutput;
use App\Entity\Document;

class DocumentOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        return DocumentOutput::createFromEntity($data);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Document) {
          return false;
        }

        return Document::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
