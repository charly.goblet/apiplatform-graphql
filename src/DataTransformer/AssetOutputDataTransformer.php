<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\AssetOutput;
use App\Entity\Asset;

class AssetOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        return AssetOutput::createFromEntity($data);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Asset) {
          return false;
        }

        return Asset::class === $to && null !== ($context['input']['class'] ?? null);
    }
}