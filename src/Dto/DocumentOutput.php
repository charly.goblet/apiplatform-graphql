<?php

namespace App\Dto;

use App\Entity\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class DocumentOutput
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var Collection<int, AssetOutput>
     */
    public $assets;

    public static function createFromEntity(Document $document): self
    {
        $dto = new self();

        $dto->id = $document->getId();
        $dto->name = $document->getName();
        foreach ($document->getAssets() as $asset) {
          $dto->assets->add(AssetOutput::createFromEntity($asset));
        }

        return $dto;
    }

    public function __construct()
    {
        $this->assets = new ArrayCollection();
    }
}
