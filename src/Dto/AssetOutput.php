<?php

namespace App\Dto;

use App\Entity\Document;
use App\Entity\Asset;

class AssetOutput
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var DocumentOutput
     */
    public $document;

    public static function createFromEntity(Asset $asset): self 
    {
        $dto = new self();

        $dto->id = $asset->getId();
        $dto->name = $asset->getName();
        $dto->document = DocumentOutput::createFromEntity($asset->getDocument());

        return $dto;
    }
}